/*Required stack functions:
 - show all the items in the stack
 - push an element in the stack
 - pop an element in the stack
 - show the top element in the stack
 - return the size of the stack
 - return if there are items in the stack or not
*/
// 'John', 'Jane', 'Bob', 'Cherry'
let collection = [];

// Write the queue functions below.

function print(){
	return collection;
}

function enqueue(name){
	collection[collection.length]=name;
	return collection;
}


function dequeue(){
	let newCollection = [];
	
	for(let i = 0; i < collection.length - 1; ++i){
		newCollection[i] = collection[i + 1];
	}

	collection = newCollection

	return collection;
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	if(collection.length > 0){
		return false;
	}
	else{
		return true;
	}
}




module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};

